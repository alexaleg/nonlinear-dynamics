using DataFrames
using Plots
gr()

data = readtable("CapDimData.dat", separator=',', header= false)
data = convert(Array, data)

function normalizeData(data)
    s = size(data)[2]
    maxVec = zeros(1,s)
    minVec = zeros(1,s)
    for i in 1:size(data)[2]
        max = maximum(data[:,i])
        min = minimum(data[:,i])
        data[:,i] = (data[:,i]-min)/(max-min)
        maxVec[i] = max
        minVec[i] = min
    end
    return data, maxVec, minVec
end

data,maxVec,minVec = normalizeData(data)
# display(scatter(data[:,1],data[:,3], xticks=0:0.01:1.0, yticks=0:0.01:1.0))


function boxCount(data, epsilon)
    nel = ceil(Int,(maxVec[1]-minVec[1])/epsilon)
    delta = 1.0/nel
    boxMatrix = zeros(nel, nel)
    for i in 1:size(data)[1]
        coor = Array{Int}(3,1)
        for j in 1:size(data)[2]
            coor[j] = ceil(Int,data[i,j]/delta)
            if coor[j] == 0
                coor[j] = 1
            end
            if coor[j] == nel+1
                coor[j]= nel
            end
        end
        boxMatrix[coor[1], coor[3]] = 1
    end
    nBoxes = sum(boxMatrix)
    # println("The number of boxes is $nBoxes")
    return nBoxes
end

function fitData(dataPlot)
    X = zeros(len+1,2)
    X[:,1] = dataPlot[:,1]
    X[:,2] = 1.0
    coeffPred = X\dataPlot[:,2]
    slope = round(coeffPred[1],4)
    # println("The capacity dimension is $slope")
    Y = X*coeffPred
    return X,Y, slope
end

function displayPlot(dataPlot, X, Y, slope)
    p = scatter(dataPlot[:,1],dataPlot[:,2], xlabel= "log(1/epsilon)",
                                             ylabel= "log(N(epsilon))",
                                             label= "Points")
    display(p)
    display(plot!(X[:,1],Y, label="Slope: $slope"))
end

function findDimension(data, emin, emax, delta, ndim=true)
    len = ceil(Int,(emax-emin)/delta)
    dataPlot = zeros(len+1,2)
    if ndim
        for n in 0:len
            epsilon = emin + n*delta
            dataPlot[n+1,1]=log10(1/epsilon)
            dataPlot[n+1,2]=log10(boxCount_nD(data,epsilon))
            end
    else
        for n in 0:len
            epsilon = emin + n*delta
            dataPlot[n+1,1]=log10(1/epsilon)
            dataPlot[n+1,2]=log10(boxCount(data,epsilon))
            end
        end
        X,Y, slope = fitData(dataPlot)
    displayPlot(dataPlot, X, Y, slope)
    return slope
end

emin = 0.3
emax = 8
delta = 0.01
ndimension = findDimension(data, emin, emax, delta, false)
dimension = findDimension(data, emin, emax, delta)
function boxCount_nD(data, epsilon)
    nel = ceil(Int,(maxVec[1]-minVec[1])/epsilon)
    delta = 1.0/nel
    dataPoints = size(data)[1]
    dims = size(data)[2]
    boxSize = Array{Int}(dims,1)
    for i in 1:dims
        boxSize[i] = nel
    end
    boxSize = tuple(boxSize ...)
    boxMatrix = zeros(boxSize)
    for i in 1:dataPoints
        coor = Array{Int}(dims,1)
        for j in 1:dims
            coor[j] = ceil(Int,data[i,j]/delta)
            if coor[j] == 0
                coor[j] = 1
            end
            if coor[j] == nel+1
                coor[j]= nel
            end
        end
        boxMatrix[coor ...] = 1
    end
    nBoxes = sum(boxMatrix)
    # println("The number of boxes is $nBoxes")
    return nBoxes
end
