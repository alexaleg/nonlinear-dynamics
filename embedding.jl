f = open("amplitude.dat")
data = readlines(f)
data = map(x->parse(Float64,x),data)


function embedData(data, m, dt)
    fin = size(data)
    embMatrix = zeros(fin[1]-(m*dt-2),m)
    j = 1
    for t = m*dt-1:fin[1]
        for i=1:m
            embMatrix[j,i] = data[t-(i-1)*dt]
        end
        j += 1
    end
    return embMatrix
end

m = 7 # Dimension
dt = 18 # Spacing


# println(size(embeddedData))
# println(typeof(embeddedData))

using Plots
gr()

for dt = 2:50
    embeddedData = embedData(data, m, dt)
    x = embeddedData[:,1]
    y = embeddedData[:,3]
    # p = plot(x,y, title="Embedded data projection for m = $(m), dt = $(dt)")
    # display(p)
    # savefig("embed$(dt).png")
end

include("boxCount.jl")
dataCount = embedData(data,2,8)
println(size(dataCount))
println(boxCount_nD(dataCount, 0.5))
