using PyPlot

function harmOscillator(x, m, beta, k, g)
  dx = zeros(2)
  dx[1] = x[2]
  dx[2] = -k/m*x[1] + beta*x[2]/m + g
  return dx
end

m = 0.5
beta = 0
k = 2
g = 0

x0 = [-1,-2]
# print(harmOscillator(x0, m, beta, k, g))

function forwardEuler(x0, ti, tend, deltat)
  N = (tend-ti)/deltat + 1
  X = zeros(length(x0), N)
  t = zeros(N)
  deltaX = zeros(length(x0))
  n = 1
  X[:,1] = x0
  t[1] = ti
  for i in ti:deltat:tend-deltat
    deltaX = harmOscillator(X[:,n], m, beta, k, g)
    X[:, n+1] = X[:,n] + deltaX*deltat
    t[n+1] = n*deltat
    n += 1
    end
  return X, t
end
# print(forwardEuler([-1.0,-2.0], 0.0, 0.5, 0.1 ))

# x_1, t = forwardEuler([-1.0,-2.0], 0.0, 0.1*200.0, 0.1 )
#
# x_2, t = forwardEuler([-1.0,-2.0], 0.0, 0.11*200.0, 0.11)

# plot(x_1[1,:],x_1[2,:])
# plot(x_2[1,:],x_2[2,:])

function backwardEuler(x0, ti, tend, deltat)
  N = (tend-ti)/deltat + 1
  X = zeros(length(x0), N)
  t = zeros(N)
  deltaX = zeros(length(x0))
  n = 1
  X[:,1] = x0
  t[1] = ti
  for i in ti:deltat:tend-deltat
    deltaX = harmOscillator(X[:,n], m, beta, k, g)
    Xfe = X[:,n] + deltaX*deltat
    deltaX = harmOscillator(Xfe, m, beta, k, g)
    X[:,n+1] = X[:,n] + deltaX*deltat
    t[n+1] = n*deltat
    n += 1
    end
  return X, t
end
# print(backwardEuler([-1.0,-2.0], 0.0, 0.5, 0.1 ))

# x_1, t = backwardEuler([-1.0,-2.0], 0.0, 0.1*200.0, 0.1 )
#
# x_2, t = backwardEuler([-1.0,-2.0], 0.0, 0.11*200.0, 0.11)

# plot(x_1[1,:],x_1[2,:])
# plot(x_2[1,:],x_2[2,:])

# x_1, t = backwardEuler([-1.0,-2.0], 0.0, 0.1*50.0, 0.1 )
#
# x_2, t = forwardEuler([-1.0,-2.0], 0.0, 0.1*50.0, 0.1)
#
# plot(x_1[1,:],x_1[2,:])
# plot(x_2[1,:],x_2[2,:])
# k = 2
# m = 1
# x_1, t = forwardEuler([-1.0,-2.0], 0.0, 0.05*10.0, 0.05 )
# print(x_1[:,end], t[end])

function trapezoidSolver(x0, ti, tend, deltat)
  N = (tend-ti)/deltat + 1
  X = zeros(length(x0), N)
  t = zeros(N)
  deltaX = zeros(length(x0))
  n = 1
  X[:,1] = x0
  t[1] = ti
  for i in ti:deltat:tend-deltat
    deltaXfe = harmOscillator(X[:,n], m, beta, k, g)
    Xfe = X[:,n] + deltaXfe*deltat
    deltaXbe = harmOscillator(Xfe, m, beta, k, g)
    X[:,n+1] = X[:,n] + deltat/2*(deltaXfe+deltaXbe)
    t[n+1] = n*deltat
    n += 1
    end
  return X, t
end

x0 = [-1.0, -2.0]
k = 2
m = 0.5
beta = 0

x_t, t = trapezoidSolver(x0,0.0,0.5,0.05)
print(x_t[:,end], t[end])
# plot(x_t[1,:],x_t[2,:])
#
# x_t, t = trapezoidSolver(x0,0.0,0.01*50000,0.01)
# # print(x_t, t)
# plot(x_t[1,:],x_t[2,:])
