


function logistic(x,r)
  y = r*x*(1-x)
  return y
end

x_0 = 0.2
x_0hat = 0.200001

r = 3.72
n = 500000

function logRun(x0,n,r)
  x_n = zeros(n+1)
  t = zeros(n+1)
  x_n[1] = x0
  t[1] = 0
  for i=2:n+1
    x_n[i] = logistic(x_n[i-1],r)
    t[i] = i
  end
  return x_n, t
end
# x_n, t = logRun(x_0,n,r)
# # scatter(t,x_test)
# x_nhat, t = logRun(x_0hat,n,r)
#
# x = abs(x_n - x_nhat)
# y = sum(x)/n
# println(y)
# # scatter(t,x)
using PyPlot

## Bifurcation diagram
function bifurcation(x0, rmin, rmax, rstep)
  n = 900
  k = 200
  one = ones(n+1)
  for i=rmin:rstep:rmax
    x, t = logRun(x0,n,i)
    scatter(i*one[k:end],x[k:end], color= :black, s=1)
  end
end
bifurcation(0.5, 3.84, 3.8571, 0.00001)
